#
# Be sure to run `pod lib lint BBBBPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BBBBPod'
  s.version          = '0.3.0'
  s.summary          = 'just for test b.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
just for test b.
                       DESC

  s.homepage         = 'https://gitee.com/redapp1890/BPod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'liyanbin0938@163.com' => 'redapp1890@163.com' }
  s.source           = { :git => 'https://gitee.com/redapp1890/BPod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'BBBBPod/Classes/**/*'
  
  # s.resource_bundles = {
  #   'BBBBPod' => ['BBBBPod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'SmartCityRouter'
  s.dependency 'APod'
end
