# SmartCityRouter

[![CI Status](https://img.shields.io/travis/liyanbin0938@163.com/SmartCityRouter.svg?style=flat)](https://travis-ci.org/liyanbin0938@163.com/SmartCityRouter)
[![Version](https://img.shields.io/cocoapods/v/SmartCityRouter.svg?style=flat)](https://cocoapods.org/pods/SmartCityRouter)
[![License](https://img.shields.io/cocoapods/l/SmartCityRouter.svg?style=flat)](https://cocoapods.org/pods/SmartCityRouter)
[![Platform](https://img.shields.io/cocoapods/p/SmartCityRouter.svg?style=flat)](https://cocoapods.org/pods/SmartCityRouter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SmartCityRouter is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SmartCityRouter'
```

## Author

liyanbin0938@163.com, redapp1890@163.com

## License

SmartCityRouter is available under the MIT license. See the LICENSE file for more info.
