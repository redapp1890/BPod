//
//  AViewController.swift
//  TestPod
//
//  Created by Red App on 2019/2/20.
//  Copyright © 2019 CEC-CESEC. All rights reserved.
//

import UIKit
import SmartCityRouter

open class AViewController: FatherVc {

    override open func viewDidLoad() {
        super.viewDidLoad()

        let v = UIView(frame: CGRect(x: 10, y: 200, width: 50, height: 50))
        v.backgroundColor = UIColor.red
        view.addSubview(v)
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.orange
    }
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let result = resultCallBackB else {
            return
        }
        let mulDict = NSMutableDictionary()
        mulDict["name"] = "Hello world"
        result(mulDict)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
