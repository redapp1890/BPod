# APod

[![CI Status](https://img.shields.io/travis/liyanbin0938@163.com/APod.svg?style=flat)](https://travis-ci.org/liyanbin0938@163.com/APod)
[![Version](https://img.shields.io/cocoapods/v/APod.svg?style=flat)](https://cocoapods.org/pods/APod)
[![License](https://img.shields.io/cocoapods/l/APod.svg?style=flat)](https://cocoapods.org/pods/APod)
[![Platform](https://img.shields.io/cocoapods/p/APod.svg?style=flat)](https://cocoapods.org/pods/APod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

APod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'APod'
```

## Author

liyanbin0938@163.com, redapp1890@163.com

## License

APod is available under the MIT license. See the LICENSE file for more info.
