//
//  BViewController.swift
//  BBBBPod_Example
//
//  Created by Red App on 2019/2/21.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import SmartCityRouter
import APod

class BViewController: FatherVc {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.lightGray
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let vc = AViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
