echo "-------------START---------------"
pwd
cd ./Example
pod install
cd ..
pwd
echo "----------------------------"
pod lib lint --no-clean --allow-warnings BBBBPod.podspec
git tag 0.3.0
git add .
git commit -m "0.3.0"
git pull
git push
git push --tag
#pod repo add BBBBPod https://gitee.com/redapp1890/BPod.git
pod repo push --allow-warnings BBBBPod BBBBPod.podspec

pod trunk --allow-warnings push BBBBPod.podspec
echo "-------------END---------------"
