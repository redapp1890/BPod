# BBBBPod

[![CI Status](https://img.shields.io/travis/liyanbin0938@163.com/BBBBPod.svg?style=flat)](https://travis-ci.org/liyanbin0938@163.com/BBBBPod)
[![Version](https://img.shields.io/cocoapods/v/BBBBPod.svg?style=flat)](https://cocoapods.org/pods/BBBBPod)
[![License](https://img.shields.io/cocoapods/l/BBBBPod.svg?style=flat)](https://cocoapods.org/pods/BBBBPod)
[![Platform](https://img.shields.io/cocoapods/p/BBBBPod.svg?style=flat)](https://cocoapods.org/pods/BBBBPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BBBBPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BBBBPod'
```

## Author

liyanbin0938@163.com, redapp1890@163.com

## License

BBBBPod is available under the MIT license. See the LICENSE file for more info.
